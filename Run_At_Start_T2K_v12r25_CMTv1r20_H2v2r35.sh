#!/bin/bash

export T2KHOME=/home/jkisiel/programs/t2k

export CVSROOT=:ext:anoncvs@repo.nd280.org:/home/trt2kmgr/ND280Repository
export CVS_RSH=ssh
unset CVS_SERVER


source $T2KHOME/CMT/setup_v1r20.sh
#---------------------------------------------------------------------


echo "#### Setting nd280..."
export CMTPATH=$T2KHOME/nd280soft/v12r25
source $T2KHOME/nd280soft/v12r25/nd280/v12r25/cmt/setup.sh

#echo "### Setting highland2..."
export CMTPATH=$T2KHOME/highland/v2r35:$T2KHOME/nd280soft/v12r25
source $T2KHOME/highland/v2r35/nd280Highland2/v2r35/cmt/setup.sh

echo "#### Done!"
